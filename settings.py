from django.conf import settings

# 设置拦截中间件
app = 'plugins.dvadmin_request_intercept.middleware.RequestInterceptMiddleware'
if app not in settings.MIDDLEWARE:
    settings.MIDDLEWARE += [app]
