from django.apps import AppConfig


class DvadminRequestInterceptConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'plugins.dvadmin_request_intercept'
